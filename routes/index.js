var express = require('express');
var router = express.Router();

/* GET home page. */
// router.get('/', isLoggedIn, function(req, res, next) {
//   res.redirect('/catalog');
// });
router.get('/catalog', isLoggedIn, function(req, res, next) {
  res.redirect('/catalog/books');
});

router.get('/vue', isLoggedIn, function(req, res, next) {
  res.render('index_vue', { title: 'Vue Js'});
});

router.get('/users', function(req, res, next) {
  res.redirect('/users');
})
module.exports = router;

function isLoggedIn(req, res, next) {

    // if user is authenticated in the session, carry on
    if (req.isAuthenticated())
        return next();

    // if they aren't redirect them to the home page
    res.redirect('/');
}
